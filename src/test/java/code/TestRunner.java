package code;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/resources",
		plugin = {"json:target/selenium-reports/cucumber.json","html:target/selenium-reports"},
		
		tags = {"~@sampleone"}       
			
		)


public class TestRunner {

}
